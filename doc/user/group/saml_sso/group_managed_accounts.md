---
type: reference, howto
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
remove_date: '2022-06-13'
redirect_to: 'index.md'
---

# Group Managed Accounts **(PREMIUM)**

This [closed beta](https://about.gitlab.com/handbook/product/gitlab-the-product/#sts=Closed%20Beta) feature was never enabled globally. See
[this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/296544) for progress on removing the feature.
Use [SAML SSO](index.md) instead.
